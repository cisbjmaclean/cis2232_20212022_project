# README #

This repository will be used to hold the requirements for the project 
to be completed for CIS2232 in 20212022AYR.  

### What is this repository for? ###

All project related details will be provided via this repository.  You will be 
using Bitbucket (git) this year in CIS1160 but will put it to use in various other courses.

### How do I get set up? ###

The code from CIS1232 will be provided in this repository.  Last year we were working with console
based applications and this will be applicable for the first couple topics in CIS2232 (Files/Database) but
we will then move to a Spring based MVC project.  

### Who do I talk to? ###

* BJ MacLean 
* Contact via Teams / email