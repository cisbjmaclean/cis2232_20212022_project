package info.hccis.sample.controllers;

import info.hccis.sample.util.CisUtility;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {

    @RequestMapping("/")
    public String home(HttpSession session) {
        
        //BJM 20200602 Issue#1 Set the current date in the session
        String currentDate = CisUtility.getCurrentDate("yyyy-MM-dd");
        session.setAttribute("currentDate", currentDate); 
        
        return "index";
    }

    @RequestMapping("/about")
    public String about() {
        return "other/about";
    }
}
