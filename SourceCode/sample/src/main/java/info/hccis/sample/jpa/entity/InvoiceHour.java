package info.hccis.sample.jpa.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents a 
 * @author bjmac
 * @since 8-Jun-2021
 */
@Entity
@Table(name = "invoicehour")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InvoiceHour.findAll", query = "SELECT i FROM InvoiceHour i")})
public class InvoiceHour implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "invoiceId")
    private Integer invoiceId;
    @Column(name = "numberOfHours")
    private Integer numberOfHours;
    @Column(name = "hoursTypeCode")
    private Integer hoursTypeCode;

    public InvoiceHour() {
    }

    public InvoiceHour(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Integer getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(Integer numberOfHours) {
        this.numberOfHours = numberOfHours;
    }

    public Integer getHoursTypeCode() {
        return hoursTypeCode;
    }

    public void setHoursTypeCode(Integer hoursTypeCode) {
        this.hoursTypeCode = hoursTypeCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvoiceHour)) {
            return false;
        }
        InvoiceHour other = (InvoiceHour) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InvoiceHour[ id=" + id + " ]";
    }

}
