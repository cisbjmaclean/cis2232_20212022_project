package info.hccis.sample.repositories;

import info.hccis.sample.jpa.entity.Tutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TutorRepository extends CrudRepository<Tutor, Integer> {
    

}