package info.hccis.sample.controllers;

import info.hccis.sample.jpa.entity.Tutor;
import info.hccis.sample.repositories.TutorRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the test functionality of the site.
 *
 * @since 20200930
 * @author CIS2232
 */
@Controller
@RequestMapping("/test")
public class TestController {

    private final TutorRepository tutorRepository;

    public TestController(TutorRepository tr) {
        tutorRepository = tr;
    }

    /**
     * Page to allow user to view sample data
     *
     * @since 20200528
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/test")
    public String list(Model model) {

        
        Tutor tutor = new Tutor();
        tutor.setEmployeeType(1);
        tutor.setFirstName("Joe");
        
        tutor = tutorRepository.save(tutor);
        System.out.println("Tutor was saved.  "
                + "Tutor="+tutor.toString());
        
        return "other/test";
    }

}
