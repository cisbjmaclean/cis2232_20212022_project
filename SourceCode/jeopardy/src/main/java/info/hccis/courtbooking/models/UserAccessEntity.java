package info.hccis.courtbooking.models;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "useraccess", schema = "cis2232_fitness")
public class UserAccessEntity {
    private int userId;
    private String username;
    private String password;
    private String lastName;
    private String firstName;
    private int userTypeCode;
    private String additional1;
    private String additional2;
    private String createdDateTime;

    @Id
    @Column(name = "userId")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "lastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "firstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "userTypeCode")
    public int getUserTypeCode() {
        return userTypeCode;
    }

    public void setUserTypeCode(int userTypeCode) {
        this.userTypeCode = userTypeCode;
    }

    @Basic
    @Column(name = "additional1")
    public String getAdditional1() {
        return additional1;
    }

    public void setAdditional1(String additional1) {
        this.additional1 = additional1;
    }

    @Basic
    @Column(name = "additional2")
    public String getAdditional2() {
        return additional2;
    }

    public void setAdditional2(String additional2) {
        this.additional2 = additional2;
    }

    @Basic
    @Column(name = "createdDateTime")
    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public void addPropertiesFromMember(MemberEntity member) {
        firstName = member.getFirstName();
        lastName = member.getLastName();
        userTypeCode = member.getUserTypeCode();
        password = member.getUserId() == 0 ? "" : password;
        username = member.getUserId() == 0 ? "" : username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccessEntity that = (UserAccessEntity) o;
        return userId == that.userId &&
                userTypeCode == that.userTypeCode &&
                Objects.equals(username, that.username) &&
                Objects.equals(password, that.password) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(additional1, that.additional1) &&
                Objects.equals(additional2, that.additional2) &&
                Objects.equals(createdDateTime, that.createdDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, username, password, lastName, firstName, userTypeCode, additional1, additional2, createdDateTime);
    }
}
