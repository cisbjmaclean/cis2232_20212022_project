package info.hccis.courtbooking.repositories;

import info.hccis.courtbooking.models.CourtTimesEntity;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;

public interface CourtTimesRepository extends CrudRepository<CourtTimesEntity, String> {
        ArrayList<CourtTimesEntity> findAllByStartTimeGreaterThanEqual(String startTime);
}
