package info.hccis.courtbooking.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {

    @RequestMapping("/")
    public String home() {
        return "index";
    }

    @RequestMapping("/about_bj")
    public String about() {
        return "other/about_bj";
    }

    @RequestMapping("/about_bj2")
    public String about2() {
        return "other/about_bj2";
    }

    @RequestMapping("/future")
    public String future() {
        return "other/future";
    }

}
