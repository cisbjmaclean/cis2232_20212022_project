/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.courtbooking.rest;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import info.hccis.courtbooking.repositories.CourtRepository;
import info.hccis.courtbooking.models.CourtEntity;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author Amro
 */
@RestController
@RequestMapping("/rest/court")
public class CourtRest {

    private final CourtRepository cr;

    @Autowired
    public CourtRest(CourtRepository cr) {
        this.cr = cr;

    }

    @RequestMapping("/")
    public ArrayList<CourtEntity> getAll() {
        return (ArrayList<CourtEntity>) cr.findAll();
    }

    @RequestMapping("/{id}")
    public CourtEntity getById(@PathVariable int id) {
        return cr.findById(id).orElseGet(CourtEntity::new);
    }

    @RequestMapping("/addcourt")
    public CourtEntity addCourt(@Valid @RequestBody CourtEntity court) {
        if (court.getId() == null) {
            court.setId(0);
        }

        return cr.save(court);
    }
}
