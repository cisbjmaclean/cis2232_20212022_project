package info.hccis.courtbooking.services;

import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Service
public class IOService {
    public <T> void saveToFile(ArrayList<T> list, String type) throws IOException {

        SimpleDateFormat date = new SimpleDateFormat("yyyymmddhhmmss");
        Path PATH = Paths.get("c:\\fitness\\" + type + "_" + date.format(new Date()) + ".json");

        if (!Files.exists(PATH)) {
            Files.createDirectories(PATH.getParent());
        }

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(String.valueOf(PATH)))) {
            System.out.println("Saving...");

            new GsonBuilder().setPrettyPrinting().create().toJson(list, bw);
        } catch (Exception e) {
            System.out.println("Saving failed.");
            System.out.println(e.getMessage());
        }
    }
}
