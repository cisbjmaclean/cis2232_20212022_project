package info.hccis.courtbooking.controllers;

import info.hccis.courtbooking.models.CodeValueEntity;
import info.hccis.courtbooking.models.CourtEntity;
import info.hccis.courtbooking.models.CourtBookingEntity;
import info.hccis.courtbooking.repositories.CodeValueRepository;
import info.hccis.courtbooking.repositories.CourtBookingRepository;
import info.hccis.courtbooking.models.CourtTimesEntity;
import info.hccis.courtbooking.repositories.CourtRepository;
import info.hccis.courtbooking.repositories.CourtTimesRepository;
import info.hccis.courtbooking.services.CourtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller which control between the HTML page and Database
 *
 * @since 20191212
 * @author Amro Daas
 */
@Controller
@RequestMapping("/courts")
public class CourtController {

    private final CourtRepository cr;
    private final CourtService cs;
    private final CodeValueRepository cv;
    private final CourtBookingRepository cbr;
    private final CourtTimesRepository ctr;

    @Autowired
    public CourtController(CourtRepository cr, CourtService cs, CodeValueRepository cv, CourtBookingRepository cbr, CourtTimesRepository ctr) {
        this.cr = cr;
        this.cs = cs;
        this.cv = cv;
        this.cbr = cbr;
        this.ctr = ctr;
    }

    /**
     * Mapping to exports the courts from database to a file
     *
     * @since 20191212
     * @author Amro Daas
     */
    @RequestMapping("/export")
    public String showCourt(HttpSession session, Model model) throws IOException {
        session.setAttribute("title", "Courts");

        ArrayList<CourtEntity> courts = (ArrayList<CourtEntity>) cr.findAll();

        model.addAttribute("court", courts);

        cs.saveCourtsToFile(courts);

        return "other/export";

    }

    /**
     * Mapping to list all the courts from database
     *
     * @since 20191212
     * @author Amro Daas
     */
    @RequestMapping("/list")
    public String showAllCourts(Model model) {

        ArrayList<CourtEntity> courts = (ArrayList<CourtEntity>) cr.findAll();

        model.addAttribute("courts", courts);

        return "courts/list";
    }

    /**
     * Mapping to add a new court to a database from HTML
     *
     * @param model
     * @param request
     * @return
     * @since 20191212
     * @author Amro Daas
     */
    @RequestMapping("/add")
    public String addACourt(Model model, HttpServletRequest request) {

        CourtEntity court = new CourtEntity();
        court.setId(0);
        ArrayList<CodeValueEntity> codeValues = (ArrayList<CodeValueEntity>) cv.findCodeValueEntitiesByCodeTypeId(2);
        model.addAttribute("types", codeValues);
        model.addAttribute("court", court);

        return "courts/add";
    }

    /**
     * Mapping to delete a court from database based on id
     *
     * @param model
     * @param request
     * @param session
     * @return
     * @since 20191212
     * @author Amro Daas
     */
    @RequestMapping("/delete")
    public String deleteACourt(Model model, HttpServletRequest request, HttpSession session) {

        session.setAttribute("title", "Courts");

        cr.deleteById(Integer.parseInt(request.getParameter("id")));

        ArrayList<CourtEntity> courts = (ArrayList<CourtEntity>) cr.findAll();

        model.addAttribute("courts", courts);

        return "courts/list";
    }

    /**
     * Mapping to update/edit a court information
     *
     * @param model
     * @param request
     * @return
     * @since 20191212
     * @author Amro Daas
     */
    @RequestMapping("/edit")
    public String editACourt(Model model, HttpServletRequest request) {

        Optional<CourtEntity> court = cr.findById((Integer.parseInt(request.getParameter("id"))));
        model.addAttribute("court", court);
        ArrayList<CodeValueEntity> codeValues = (ArrayList<CodeValueEntity>) cv.findCodeValueEntitiesByCodeTypeId(2);
        model.addAttribute("types", codeValues);

        return "courts/add";
    }

    /**
     * Mapping to add and save a court information and save it to database
     *
     * @param model
     * @param session
     * @param court
     * @param result
     * @return
     * @since 20191212
     * @author Amro Daas
     */
    @RequestMapping("/addSubmit")
    public String addSubmitACourt(Model model, HttpSession session, @Valid @ModelAttribute("court") CourtEntity court, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Validation Error";
            model.addAttribute("message", error);

            ArrayList<CodeValueEntity> codeValues = (ArrayList<CodeValueEntity>) cv.findCodeValueEntitiesByCodeTypeId(2);
            model.addAttribute("types", codeValues);

            return "courts/add";
        }
        try {
            cr.save(court);

        } catch (Exception ex) {
            Logger.getLogger(CourtController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "redirect:/courts/list";

    }

    /**
     * Mapping to take the input from the user and the available courts(under
     * construction)
     *
     * @param model
     * @return
     * @since 20191212
     * @author Amro Daas
     */
    @RequestMapping("/search")
    public String searchCourt(Model model) {
        ArrayList<CodeValueEntity> codeValues = (ArrayList<CodeValueEntity>) cv.findCodeValueEntitiesByCodeTypeId(2);
        model.addAttribute("types", codeValues);
        return "courts/courtsSearch";
    }

    /**
     * Mapping to get the available courts(under construction)
     *
     * @param model
     * @param date
     * @param time
     * @param courtType
     * @return
     * @since 20191212
     * @author Amro Daas
     */
    @RequestMapping("/searchResult")
    public String ShowAvalibleCourts(Model model, @RequestParam String date, @RequestParam String time, @RequestParam int courtType) {
        int courtNumber = 0;
        ArrayList<CourtEntity> avCourts = new ArrayList<>();
        ArrayList<CodeValueEntity> codeValues = (ArrayList<CodeValueEntity>) cv.findCodeValueEntitiesByCodeTypeId(2);
        ArrayList<CourtBookingEntity> bookingCourts = (ArrayList<CourtBookingEntity>) cbr.findByBookingDateAndStartTimeAfter(date, time);
        ArrayList<CourtTimesEntity> times = (ArrayList<CourtTimesEntity>) ctr.findAllByStartTimeGreaterThanEqual(time);
        ArrayList<CourtEntity> sameType = (ArrayList<CourtEntity>) cr.findAllBycourtType(courtType);
        for (CourtBookingEntity current1 : bookingCourts) {
            courtNumber = current1.getCourtNumber();
            for (CourtEntity court : sameType) {
                if (courtNumber != court.getCourtNumber()) {
                    for (CodeValueEntity codeValue : codeValues) {
                        if (court.getCourtType() == codeValue.getCodeValueSequence()) {

                            court.setCourtTypeName(codeValue.getEnglishDescription());
                        }
                    }
                    avCourts.add(court);
                }
            }
        }

        model.addAttribute("types", codeValues);
        model.addAttribute("courts", avCourts);

        return "courts/courtsAvailable";
    }
}
