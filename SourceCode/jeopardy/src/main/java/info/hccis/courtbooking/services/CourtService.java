package info.hccis.courtbooking.services;

import com.google.gson.Gson;
import info.hccis.courtbooking.models.CourtEntity;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

@Service
public class CourtService {

   public static void saveCourtsToFile(ArrayList<CourtEntity> courts) throws IOException {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
        String fileName = "c:\\fitness\\courts_" + (formatter.format(date)) + ".json";
        Path path = Paths.get(fileName);
        Files.createDirectories(path.getParent());
        FileWriter fw = null;
        BufferedWriter bw = null;

        try {

            fw = new FileWriter(fileName, true);
            bw = new BufferedWriter(fw);
            Gson gson = new Gson();
            String stringJson = "";
            for (CourtEntity current : courts) {
                stringJson += gson.toJson(current) + System.lineSeparator();
            }
            fw.write(stringJson);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }
        }
    }
}