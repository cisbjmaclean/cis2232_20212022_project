package info.hccis.courtbooking.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;

@Entity
@Table(name = "courtbooking")
public class CourtBookingEntity implements Serializable {

    //    private static final long serialVersionUID = 1L;
    @Id
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "courtNumber")
    private int courtNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "bookingDate")
    private String bookingDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "startTime")
    private String startTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "memberId")
    private int memberId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "memberIdOpponent")
    private int memberIdOpponent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "notes")
    private String notes;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "createdDate")
    private String createdDate;


    @Transient
    private String memberName;
    @Transient
    private String opponentName;

    public CourtBookingEntity() {
    }

    public CourtBookingEntity(Integer id) {
        this.id = id;
    }

    public CourtBookingEntity(Integer id, int courtNumber, String bookingDate, String startTime, int memberId, int memberIdOpponent, String notes, String createdDate) {
        this.id = id;
        this.courtNumber = courtNumber;
        this.bookingDate = bookingDate;
        this.startTime = startTime;
        this.memberId = memberId;
        this.memberIdOpponent = memberIdOpponent;
        this.notes = notes;
        this.createdDate = createdDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCourtNumber() {
        return courtNumber;
    }

    public void setCourtNumber(int courtNumber) {
        this.courtNumber = courtNumber;
    }

    public String getBookingDate() {
        if (bookingDate != null && !bookingDate.isEmpty())
            return bookingDate.substring(0, 4) + "-" + bookingDate.substring(4, 6) + "-" + bookingDate.substring(6);
        else
            return "";
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate.replace("-", "");
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getMemberIdOpponent() {
        return memberIdOpponent;
    }

    public void setMemberIdOpponent(int memberIdOpponent) {
        this.memberIdOpponent = memberIdOpponent;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }

    public CourtBookingEntity addTransientFields(ArrayList<UserAccessEntity> users) {
        UserAccessEntity user = users.stream().filter(u -> u.getUserId() == memberId).findFirst().get();
        memberName = user.getFirstName() + " " + user.getLastName();
        user = users.stream().filter(u -> u.getUserId() == memberIdOpponent).findFirst().get();
        opponentName = user.getFirstName() + " " + user.getLastName();
        return this;
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CourtBookingEntity)) {
            return false;
        }
        CourtBookingEntity other = (CourtBookingEntity) object;

        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.Courtbooking[ id=" + id + " ]";
    }

}
