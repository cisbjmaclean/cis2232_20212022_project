package info.hccis.courtbooking.controllers;

import info.hccis.courtbooking.models.CourtBookingEntity;
import info.hccis.courtbooking.repositories.CourtBookingRepository;
import info.hccis.courtbooking.repositories.CourtRepository;
import info.hccis.courtbooking.repositories.CourtTimesRepository;
import info.hccis.courtbooking.repositories.UserAccessRepository;
import info.hccis.courtbooking.services.CourtBookingService;
import info.hccis.courtbooking.services.IOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * This whole controller was just done to practice. Mostly working.
 *
 * @author Fred Campos
 * @since 20191212
 */
@Controller
@RequestMapping("/courtBookings")
public class CourtBookingController {

    private final CourtBookingRepository _cbr;
    private final UserAccessRepository _uar;
    private final CourtTimesRepository _ctr;
    private final IOService _ios;
    private final CourtBookingService _cbs;
    private final CourtRepository _cr;

    @Autowired
    public CourtBookingController(CourtBookingRepository cbr, UserAccessRepository uar, CourtTimesRepository ctr, IOService ios, CourtBookingService cbs, CourtRepository cr) {
        _cbr = cbr;
        _uar = uar;
        _ctr = ctr;
        _ios = ios;
        _cbs = cbs;
        _cr = cr;
    }

    @RequestMapping("")
    public String bookCourt(Model model) {
        model.addAttribute("booking", new CourtBookingEntity());
        model.addAttribute("users", _uar.findAll());
        model.addAttribute("times", _ctr.findAll());
        model.addAttribute("courts", _cr.findAll());
        return "courtBookings/courtBooking";
    }

    @RequestMapping("/submit")
    public String submit(Model model, @Valid @ModelAttribute("booking") CourtBookingEntity booking,
                         BindingResult bindingResult) {

        SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
        String dateee = date.format(new Date());
        booking.setCreatedDate(date.format(new Date()));

        if (_cbs.hasMultipleBookings(booking))
            bindingResult.rejectValue("memberId", "", "Member already has 2 future bookings");

        if (_cbs.hasSameDayBooking(booking))
            bindingResult.rejectValue("courtNumber", "", "This court is already booked at this time");


        // must be 2 different members
        if (booking.getMemberId() == booking.getMemberIdOpponent())
            bindingResult.rejectValue("memberIdOpponent", "", "Must be 2 different members");

        if (bindingResult.hasErrors()) {
            System.out.println("Validation error");
            for (ObjectError error : bindingResult.getAllErrors()) {
                model.addAttribute("users", _uar.findAll());
                model.addAttribute("times", _ctr.findAll());
                model.addAttribute("courts", _cr.findAll());
                System.out.println(error.getDefaultMessage());
            }
            return "courtBookings/courtBooking";
        }

        _cbr.save(booking);
        return "redirect:/";
    }


    @RequestMapping("/export")
    public String export(HttpSession session, Model model) throws IOException {
        session.setAttribute("title", "Court Booking");
        ArrayList<CourtBookingEntity> courtBookings = (ArrayList<CourtBookingEntity>) _cbr.findAll();
        _ios.saveToFile(courtBookings, "court-bookings");

        return "other/export";
    }
}
