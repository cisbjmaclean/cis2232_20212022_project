package info.hccis.courtbooking.models;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "FitnessMember", schema = "cis2232_fitness")
public class MemberEntity {
    @Id
    private int userId;
    private String phoneCell;
    private String phoneHome;
    private String phoneWork;
    private String address;
    private int status;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String membershipStartDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String membershipEndDate;


    private String firstName;


    private String lastName;

    @Transient
    private int userTypeCode = 1;

    @Basic
    @Column(name = "userId", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "phoneCell")
    @Size(min = 10, max = 10, message = "Phone must be 10 digits")
    @NotEmpty(message = "You must enter a phone number")
    public String getPhoneCell() {
        return phoneCell;
    }

    public void setPhoneCell(String phoneCell) {
        this.phoneCell = phoneCell;
    }

    @Basic
    @Column(name = "phoneHome", nullable = false, length = 10)
    @Size(min = 10, max = 10, message = "Phone must be 10 digits")
    @NotEmpty(message = "You must enter a phone number")
    public String getPhoneHome() {
        return phoneHome;
    }

    public void setPhoneHome(String phoneHome) {
        this.phoneHome = phoneHome;
    }

    @Basic
    @Column(name = "phoneWork", nullable = false, length = 10)
    @Size(min = 10, max = 10, message = "Phone must be 10 digits")
    @NotEmpty(message = "You must enter a phone number")
    public String getPhoneWork() {
        return phoneWork;
    }

    public void setPhoneWork(String phoneWork) {
        this.phoneWork = phoneWork;
    }

    @Basic
    @Column(name = "address", nullable = false, length = 100)
    @Size(max = 100, message = "Address must be less than 100 characters")
    @NotEmpty(message = "You must enter an address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @NotEmpty(message = "You must enter a date")
    @Column(name = "membershipStartDate", length = 8)
    public String getMembershipStartDate() {
        if (membershipStartDate != null && !membershipStartDate.isEmpty())
            return membershipStartDate.substring(0, 4) + "-" + membershipStartDate.substring(4, 6) + "-" + membershipStartDate.substring(6);
        else
            return "";
    }

    public String getUnFormattedStartDate() {
        return membershipStartDate;
    }

    public void setMembershipStartDate(String membershipStartDate) {
        this.membershipStartDate = membershipStartDate.replace("-", "");
//            this.membershipStartDate = membershipStartDate;
    }

    @Basic
    @NotEmpty(message = "You must enter a date")
    @Column(name = "membershipEndDate", length = 8)
    public String getMembershipEndDate() {
        if (membershipEndDate != null && !membershipEndDate.isEmpty())
            return membershipEndDate.substring(0, 4) + "-" + membershipEndDate.substring(4, 6) + "-" + membershipEndDate.substring(6);
        else
            return "";
    }

    public String getUnFormattedEndDate() {
        return membershipEndDate;
    }

    public void setMembershipEndDate(String membershipEndDate) {
        this.membershipEndDate = membershipEndDate.replace("-", "");
//            this.membershipEndDate = membershipEndDate;

    }

    public void addTransientFields(UserAccessEntity user) {
        firstName = user.getFirstName();
        lastName = user.getLastName();
    }

    @Basic
    @Transient
    @NotEmpty(message = "Name cannot be empty")
    @Size(max = 100, message = "Name must be less than 100 characters")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    @Transient
    @NotEmpty(message = "Name cannot be empty")
    @Size(max = 100, message = "Name must be less than 100 characters")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getUserTypeCode() {
        return userTypeCode;
    }

    public void setUserTypeCode(int userTypeCode) {
        this.userTypeCode = userTypeCode;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MemberEntity that = (MemberEntity) o;

        if (userId != that.userId) return false;
        if (status != that.status) return false;
        if (phoneCell != null ? !phoneCell.equals(that.phoneCell) : that.phoneCell != null) return false;
        if (phoneHome != null ? !phoneHome.equals(that.phoneHome) : that.phoneHome != null) return false;
        if (phoneWork != null ? !phoneWork.equals(that.phoneWork) : that.phoneWork != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (membershipStartDate != null ? !membershipStartDate.equals(that.membershipStartDate) : that.membershipStartDate != null)
            return false;
        if (membershipEndDate != null ? !membershipEndDate.equals(that.membershipEndDate) : that.membershipEndDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (phoneCell != null ? phoneCell.hashCode() : 0);
        result = 31 * result + (phoneHome != null ? phoneHome.hashCode() : 0);
        result = 31 * result + (phoneWork != null ? phoneWork.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + status;
        result = 31 * result + (membershipStartDate != null ? membershipStartDate.hashCode() : 0);
        result = 31 * result + (membershipEndDate != null ? membershipEndDate.hashCode() : 0);
        return result;
    }
}
