package info.hccis.courtbooking.repositories;

import info.hccis.courtbooking.models.CourtEntity;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourtRepository extends CrudRepository<CourtEntity, Integer> {
    ArrayList<CourtEntity> findAllBycourtType(int courtType);
    ArrayList<CourtEntity> findOneBycourtNumber(int courtNumber);
    ArrayList<CourtEntity> findAllBycourtNumber(int courtNum);
}
