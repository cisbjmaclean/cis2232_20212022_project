package info.hccis.courtbooking.services;

import info.hccis.courtbooking.models.CourtBookingEntity;
import info.hccis.courtbooking.models.MemberEntity;
import info.hccis.courtbooking.models.UserAccessEntity;
import info.hccis.courtbooking.repositories.CourtBookingRepository;
import info.hccis.courtbooking.repositories.MemberRepository;
import info.hccis.courtbooking.repositories.UserAccessRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Service
public class MemberService {


    private final UserAccessRepository _uar;
    private final MemberRepository _mr;
    private final CourtBookingRepository _cbr;

    public MemberService(@Qualifier("userAccessRepository") UserAccessRepository uar, @Qualifier("memberRepository") MemberRepository mr, CourtBookingRepository cbr) {
        _uar = uar;
        _mr = mr;
        _cbr = cbr;
    }

    public ArrayList<MemberEntity> getAllMembers() {
        return (ArrayList<MemberEntity>) _mr.findAll();
    }

    public ArrayList<MemberEntity> createMemberTransient() {
        ArrayList<MemberEntity> members = (ArrayList<MemberEntity>) _mr.findAll();
        ArrayList<UserAccessEntity> users = (ArrayList<UserAccessEntity>) _uar.findAll();
        return members.stream().map(member -> {
            UserAccessEntity user = _uar.findById(member.getUserId()).get();
            member.addTransientFields(user);
            return member;
        }).collect(Collectors.toCollection(ArrayList::new));
    }

    public MemberEntity createMemberTransient(int id) {
        UserAccessEntity user = _uar.findById(id).orElseGet(null);
        MemberEntity member = _mr.findById(id).orElseGet(null);

        if (user != null && member != null) {
            member.addTransientFields(user);
            return member;
        }
        return null;
    }

    public MemberEntity saveUserMember(MemberEntity member) {
        int userId = member.getUserId();
        UserAccessEntity user = (userId > 0) ? _uar.findById(userId).get() : new UserAccessEntity();
        user.addPropertiesFromMember(member);
        _uar.save(user);
        if (userId == 0) {
            user = _uar.findTopByFirstNameAndLastNameOrderByUserIdDesc(user.getFirstName(), user.getLastName());
            member.setUserId(user.getUserId());
        }
        _mr.save(member);
        return member;
    }

    public void deleteUserMember(int id) {
        _uar.deleteById(id);
        _mr.deleteById(id);
    }

    public ArrayList<CourtBookingEntity> findMemberBookings(int userId, String start, String end) {
        ArrayList<CourtBookingEntity> courtBookings = null;
        ArrayList<UserAccessEntity> users = (ArrayList<UserAccessEntity>) _uar.findAll();
        if (start.isEmpty() && end.isEmpty()) {
            courtBookings = _cbr.findByMemberId(userId);
        } else {
            courtBookings = _cbr.findByMemberIdAndBookingDateAfterAndBookingDateBefore(userId, removeHyphens(start), removeHyphens(end));
        }

        return courtBookings.stream().map(booking -> booking.addTransientFields(users)).collect(Collectors.toCollection(ArrayList::new));
    }

    private String removeHyphens(String input) {
        return input.replace("-", "");
    }
}
