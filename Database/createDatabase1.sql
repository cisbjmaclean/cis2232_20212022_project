DROP DATABASE IF EXISTS cis2232_tutor;
CREATE DATABASE cis2232_tutor;
use cis2232_tutor;

CREATE TABLE CodeType (codeTypeId int(3) COMMENT 'This is the primary key for code types',
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) COMMENT 'This tables holds the code types that are available for the application';

INSERT INTO CodeType (CodeTypeId, englishDescription, frenchDescription, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 'User Types', 'User Types FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(2, 'Status Types', 'Status Types FR', CURRENT_TIMESTAMP, '', CURRENT_TIMESTAMP, ''),
(3, 'Skill Types', 'Skill Types FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(4, 'Hour Types', 'Hour Types FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(5, 'Hour Rates', 'Hour Rates FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(6, 'Employee Types', 'Employee Types FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(7, 'Program Code', 'Program Code FR', sysdate(), '', CURRENT_TIMESTAMP, '');

CREATE TABLE CodeValue (
  codeTypeId int(3) NOT NULL COMMENT 'see code_type table',
  codeValueSequence int(3) NOT NULL,
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  englishDescriptionShort varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  frenchDescriptionShort varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  sortOrder int(3) DEFAULT NULL COMMENT 'Sort order if applicable',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) COMMENT='This will hold code values for the application.';

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 1, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');
INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 2, 'Admin', 'Admin', 'Admin', 'Admin', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(2, 1, 'Active', 'Active', 'Active', 'Active', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(2, 2, 'Not Active', 'Not Active', 'Not Active', 'Not Active', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(3, 1, 'Java', 'Java', 'Java', 'Java', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 2, 'Javascript', 'Javascript', 'Javascript', 'Javascript', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 3, 'C#', 'C#', 'C#', 'C#', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 4, 'Database', 'Database', 'Database', 'Database', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 5, 'Networking', 'Networking', 'Networking', 'Networking', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, frenchDescription, englishDescriptionShort,frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(4, 1, 'Individual', 'Individual', 'Individual', 'Individual', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(4, 2, 'Small Group', 'Small Group', 'Small Group', 'Small Group', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(4, 3, 'Large Group', 'Large Group', 'Large Group', 'Large Group', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(5, 1, '15', '15', '15', '15', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(5, 2, '18', '18', '18', '18', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(5, 3, '20', '20', '20', '20', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, frenchDescription, englishDescriptionShort,frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(6, 1, 'Second Year Student', 'Second Year Student', 'Second Year', 'Second Year', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(6, 2, 'Past Graduate', 'Past Graduate', 'Graduate', 'Graduate', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, frenchDescription, englishDescriptionShort,frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(7, 1, 'Computer Information Systems', 'Computer Information Systems', 'CIS', 'CIS', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(7, 2, 'Business Administration', 'Business Administration', 'Bus Admin', 'Bus Admin', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(7, 3, 'Computer Networking', 'Computer Networking', 'CNT', 'CNT', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(7, 4, 'Electronic Engineering', 'Electronic Engineering', 'EE', 'EE', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

CREATE TABLE UserAccess (
  userAccessId int(3) NOT NULL,
  username varchar(100) NOT NULL COMMENT 'Unique user name for app',
  password varchar(128) NOT NULL,
  name varchar(128),
  userAccessStatusCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #2',
  userTypeCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  createdDateTime datetime DEFAULT NULL COMMENT 'When user was created.'
);

ALTER TABLE CodeType
  ADD PRIMARY KEY (codeTypeId);

ALTER TABLE CodeValue
  ADD PRIMARY KEY (codeTypeId,codeValueSequence);
--  ADD KEY codeTypeId (codeTypeId);

ALTER TABLE UserAccess
  ADD PRIMARY KEY (userAccessId),
  ADD KEY userTypeCode (userTypeCode);

ALTER TABLE CodeType
  MODIFY codeTypeId int(3) NOT NULL COMMENT 'This is the primary key for code types';

ALTER TABLE CodeValue
  MODIFY codeValueSequence int(3) NOT NULL;

ALTER TABLE UserAccess
  MODIFY userAccessId int(3) NOT NULL AUTO_INCREMENT;
