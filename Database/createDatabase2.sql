use cis2232_tutor;

drop table if exists TutorSkill;
drop table if exists StudentSkill;
drop table if exists InvoiceHour;

drop table if exists Tutor;
drop table if exists Student;
drop table if exists Invoice;

CREATE TABLE Tutor(
id int(5) COMMENT 'This is the primary key',
firstName varchar(40) COMMENT 'First name',
lastName varchar(40) COMMENT 'Last name',
phoneNumber varchar(10) COMMENT '1112223333',
emailAddress varchar(100) COMMENT 'Email Address',
address varchar(100) COMMENT 'Mailing Address',
employeeType int(3) COMMENT 'Code type#6'
) COMMENT 'This table holds person data';

ALTER TABLE Tutor
  ADD PRIMARY KEY (id);
ALTER TABLE Tutor
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;

CREATE TABLE TutorSkill(
id int(5),
tutorId int(5) COMMENT 'FK to Tutor',
skillType int(3) COMMENT 'Code Type 3 (Skill where help can be provided)'
) COMMENT 'This table holds person data';

ALTER TABLE TutorSkill
  ADD PRIMARY KEY (id);
ALTER TABLE TutorSkill
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;


CREATE TABLE Student(
id int(5),
firstName varchar(40) COMMENT 'First name',
lastName varchar(40) COMMENT 'Last name',
phoneNumber varchar(10) COMMENT '1112223333',
emailAddress varchar(100) COMMENT 'Email Address',
programCode int(3) COMMENT 'Code Type#7 Student program'
) COMMENT 'This table holds tutor data';

ALTER TABLE Student
  ADD PRIMARY KEY (id);
ALTER TABLE Student
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;


CREATE TABLE StudentSkill(
id int(5),
studentId int(5) COMMENT 'FK to Student',
skillType int(3) COMMENT 'Code Type 3 (Skill where help is needed)' 
) COMMENT 'This table holds student data';

ALTER TABLE StudentSkill
  ADD PRIMARY KEY (id);
ALTER TABLE StudentSkill
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;

CREATE TABLE Invoice(
id int(5),
tutorId int(5) COMMENT 'FK to Tutor',
startDate varchar(10) COMMENT 'yyyy-mm-dd',
endDate varchar(10) COMMENT 'yyyy-mm-dd'
) COMMENT 'This table holds invoice data';

ALTER TABLE Invoice
  ADD PRIMARY KEY (id);

ALTER TABLE Invoice
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;

CREATE TABLE InvoiceHour(
id int(5) COMMENT 'This is the primary key',
invoiceId int(5) COMMENT 'FK to Invoice',
numberOfHours int(3) COMMENT 'Number of hours worked',
hoursTypeCode int(3) COMMENT 'Code Type=4'
) COMMENT 'This table holds the hour details for the invoice';

ALTER TABLE InvoiceHour
  ADD PRIMARY KEY (id);

ALTER TABLE InvoiceHour
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;

insert into Tutor values(0, "John", "Rogers", "9023145566","jrogers@gmail.com","24 MacBeth Cres. Charlottetown",1);
insert into Tutor values(0, "Bill", "Akatti", "9023146677","bakatti@gmail.com","75 Grafton St. Charlottetown",2);
insert into Tutor values(0, "George", "Ryan", "9023147789","gryan@gmail.com","12 Hillsborough St. Charlottetown",2);
insert into Tutor values(0, "Mariana", "Clark", "9023141245","mclark@hollandcollege.com","44 Water St. Charlottetown",1);
insert into Tutor values(0, "Renee", "Riley", "9029401234","jrogers@gmail.com","23 Water St. Summerside",2);

insert into Student values(0, "Ben", "Leonard", "9023148712","bleonard@hollandcollege.com",1);
insert into Student values(0, "Adline", "Sloan", "9023141234","asloan@hollandcollege.com",3);
insert into Student values(0, "Bert", "Brothers", "9023885522","bbrothers123@hollandcollege.com",3);
insert into Student values(0, "Hunter", "Ripley", "9023145432","rripley@hollandcollege.com",2);
insert into Student values(0, "Andres", "MacDonald", "9023148899","amacdonald12123@hollandcollege.com",1);

insert into Invoice values(0, 1, "2020-01-01","2020-01-31");
insert into Invoice values(0, 2, "2020-01-01","2020-01-31");
insert into Invoice values(0, 3, "2020-01-01","2020-01-31");
insert into Invoice values(0, 1, "2020-02-01","2020-01-31");
insert into Invoice values(0, 2, "2020-02-01","2020-02-15");
insert into Invoice values(0, 3, "2020-02-01","2020-02-15");

insert into InvoiceHour values (0,1,3,2); 
insert into InvoiceHour values (0,1,2,3);
insert into InvoiceHour values (0,2,3,1);
insert into InvoiceHour values (0,2,3,2);
insert into InvoiceHour values (0,2,3,3);
insert into InvoiceHour values (0,3,5,1);
insert into InvoiceHour values (0,4,10,1);
insert into InvoiceHour values (0,5,6,2);
insert into InvoiceHour values (0,5,2,3);
insert into InvoiceHour values (0,6,1,1);

insert into StudentSkill values (0,1,3);
insert into StudentSkill values (0,1,4);
insert into StudentSkill values (0,1,5);
insert into StudentSkill values (0,2,1);
insert into StudentSkill values (0,2,2);
insert into StudentSkill values (0,3,3);
insert into StudentSkill values (0,4,1);
insert into StudentSkill values (0,4,2);
insert into StudentSkill values (0,4,3);
insert into StudentSkill values (0,4,4);
insert into StudentSkill values (0,4,5);
insert into StudentSkill values (0,5,1);
insert into StudentSkill values (0,5,2);

insert into TutorSkill values (0,1,3);
insert into TutorSkill values (0,1,5);
insert into TutorSkill values (0,2,1);
insert into TutorSkill values (0,2,3);
insert into TutorSkill values (0,4,1);
insert into TutorSkill values (0,4,2);
insert into TutorSkill values (0,4,3);
insert into TutorSkill values (0,5,1);
insert into TutorSkill values (0,5,2);
